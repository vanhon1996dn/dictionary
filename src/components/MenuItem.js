import React, { Component } from 'react';
import { View, Text, TouchableOpacity } from 'react-native';
import Icon from "react-native-vector-icons/Ionicons";

class MenuItem extends Component {
  constructor(props) {
    super(props);
    this.state = {
    };
  }

  render() {
    const { container, textMenu } = styles;
      const { title, nameIcon } = this.props;
    return (
      <TouchableOpacity style={container}>
        <Icon name={nameIcon} color={"#000"} size={25} />
        <Text style={textMenu}>{title}</Text>
      </TouchableOpacity>
    );
  }
}

const styles = {
  container: {
    flexDirection: "row",
    alignItems: "center",
    marginVertical: 15,
  },
  textMenu: {
      marginLeft: 30,
  }
};

export default MenuItem;
